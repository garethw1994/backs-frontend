import React, { Component } from 'react'
import API from '../../../../resource/API';
import { Row, Col, Button, Icon, Divider, Tag, message, Input, List } from 'antd';
import util from '../../../../resource/utilities';

export default class UserList extends Component {
    constructor(props) {
        super(props)
        this.state = {
            users: [],
            staticList: [],
            columns: [
                { title: 'Employee', dataIndex: 'username', key: `${Math.random() * 1000}` },
                {title: 'Role', dataIndex: 'role', key: `_id`},
            ],
            visible: false
        }
    }

    getAllUsers = () => {
        API(`allUsers/${this.props.user.username}`, 'GET')
        .then((result) => {
            result.users.map((user) => {
                user.username = util().firstLetterUppercase(user.username);
                user.role = util().firstLetterUppercase(user.role);
            });

            console.log(result)
            this.setState({
                users: result.users,
                staticList: result.users
            })
        })
        .catch((error) => {
            console.log(error)
        })
    }
    componentWillMount = () => {
      this.getAllUsers()
    };

    removeUser = (user) => {
        API('removeUser', 'POST', { id: user._id })
            .then((result) => {
                message.success(result.message);

                // reload user list
                this.getAllUsers()
            })
                .catch((error) => {
                    message.error('Unable To Delete User.');
                    console.log(error);
                });
    };

    filterUsers = (searchParams) => {
        let users = this.state.users;

        let filteredUsers = users.filter((user) => {
            let username = user.username.toLowerCase();
            let role = user.role.toLowerCase();

            return username.includes(searchParams) || role.includes(searchParams) ? user : null;
        });

        filteredUsers.length > 0 ? this.setState({ users: filteredUsers }) : this.setState({ users: this.state.staticList });
    };

    handleSearch = (e) => {
        let search = e.target.value.toLowerCase();
        search.length > 0 ? this.filterUsers(search) : this.setState({ users: this.state.staticList });
    };

  render() {
    return (
      <div style={{ padding: '10px' }}>
        {
            this.state.users.length > 0 ?
            <Row>
                 <Col>
                    <Divider><Tag color="#B49328">USERS</Tag></Divider>
                 </Col>

                 <Col>
                    <Input.Search style={{ padding: '10px' }} placeholder="Search..." onChange={this.handleSearch}/>
                </Col>
    
                
                <Col>
                    <List 
                     dataSource={this.state.users}
                     renderItem={user => (
                        <List.Item actions={[<Button onClick={() => this.removeUser(user)} type="danger"><Icon type="delete" /></Button>]}>
                        <List.Item.Meta
                            avatar={
                            <Icon type="user"/>
                            }
                            title={ user.username }
                            description={user.role}
                        />
                        </List.Item>
                     )}
                    />
                </Col>

                <Row style={{ position: 'fixed', bottom: 0, left: '1px', width: '100%', padding: '10px' }}>
                    <Col>
                        <Button onClick={() => this.props.goBack('dashboard')} style={{ width: '100%', background: '#CCCCCC', marginTop: '10px' }}>Back</Button>            
                    </Col>
                </Row>
            </Row>       
            : 
            <Row>
                <Col>
                    <Input.Search style={{ padding: '10px' }} placeholder="Search..." onChange={this.handleSearch}/>
                </Col>

                <Row><Divider><Tag color="#B49328">No Users Found.</Tag></Divider></Row>

                <Row style={{ position: 'fixed', bottom: 0, left: '1px', width: '100%', padding: '10px' }}>
                    <Col>
                        <Button onClick={() => this.props.goBack('dashboard')} style={{ width: '100%', background: '#CCCCCC', marginTop: '10px' }}>Back</Button>            
                    </Col>
                </Row>
            </Row>
        }
      </div>
    )
  }
}