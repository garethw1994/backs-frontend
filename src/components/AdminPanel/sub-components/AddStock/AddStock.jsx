import React, { Component } from 'react'
import { Button, Row, Col, Divider, Form, Input, Icon, Upload, Checkbox, Tag, Select } from 'antd';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faWineBottle } from '@fortawesome/free-solid-svg-icons';
import API from '../../../../resource/API';
import './AddStock.css';


export default class AddStock extends Component {
    constructor(props) {
        super(props)
        this.state = {
            code: '',
            locations: [],
            locationList: [
                {label: 'Premium Range', value: 'Premium Range'},
                {label: 'Family Reserve', value: 'Family Reserve'},
                {label: 'Behind the Bar', value: 'Behind the Bar'},
                {label: 'Fridges', value: 'Fridges'},
                {label: 'Black Label Range', value: 'Black Label Range'},
                {label: 'Kosher Range', value: 'Kosher Range'},
                {label: 'Premium Range', value: 'Storage Room - Premium Range'},
                {label: 'Family Range ', value: 'Storage Room - Family Range'},
                {label: 'Black Label Range', value: 'Storage Room - Black Label Range'},
                {label: 'Kosher Range', value: 'Storage Room - Kosher Range'},
                {label: 'Thread Lightly Range', value: 'Storage Room - Thread Lightly Range'},
                {label: 'Brandy', value: 'Storage Room - Brandy'},
                {label: 'Magnums', value: 'Storage Room - Magnums'},
                {label: 'Tasting Floor', value: 'Tasting Floor'},
                {label: 'Annex', value: 'Annex'},
                {label: 'Dry/Pro Stock', value: 'Dry/Promo Stock'}
            ],
            name: '',
            fileList: [],
            file: [],
            base64Image: '',
            feedback: {error: false, message: ''},
            view: 'addStock'
        }
    };

    onCodeChange = (e) => this.setState({ code: e.target.value });

    onLocationChange = (e) => {
        let selectedVal = e.target.value;
        let selectedLocations = this.state.locations;

        if (selectedVal) {
            if (selectedLocations.length > 0) {
                // check if item already exists in list
                let exist = selectedLocations.indexOf(selectedVal);
                
                if (exist < 0) {
                    // add item
                    selectedLocations.push(selectedVal)
                } else {
                    // remove item
                    for (let i = 0; i < selectedLocations.length; i++) {
                        if (selectedLocations[i] === selectedVal) {
                            selectedLocations.splice(i, 1)
                        };
                    };
                }
            } else {
                selectedLocations.push(selectedVal);
            }
        }

        this.setState({
            locations: selectedLocations
        })
    };

    onNameChange = (e) => this.setState({ name: e.target.value });
    
    base64Converter = (file) => {
        return new Promise((resolve, reject) => {
            let origFile = file.originFileObj;

            let reader = new FileReader();
            
            reader.readAsDataURL(origFile);

            reader.onload = () => resolve(reader.result);
            
            reader.onerror = (error) => reject(error)
        });
    };

    beforeUpload = (file) => {
        if (file && file.type.match('image.*')) {
          return true;  
        };

        this.setState({ fileList: [] });
        return false;
    };
    
    onFileChange = ({ file }) => {
        this.setState({ fileList: [file] });

        if (file.status !== 'uploading') {
            // convert file to base64 && update the state
            this.base64Converter(file)
                .then((result) => {
                    this.setState({ file: file.originFileObj, base64Image: result, feedback: { error: false, message: 'Image Supported.' } });
                })
                .catch((error) => {
                    this.setState({fileList: [], feedback: { error: true, message: 'Image Not Supported.' } });
                });
        };
    };

    locationsCounter = (locations) => {
        let locationsCounterMap = [];

        locations.map((location) => {
            let locationObject = {
                location,
                packSize: [{
                    type: "bottles",
                    count: 0
                }, {
                    type: "packs",
                    count: 0
                }, {
                    type: "boxes",
                    count: 0
                }]
            };

            locationsCounterMap.push(locationObject);
            return true;
        });

        return locationsCounterMap;
    };

    handleSave = () => {
        let formData = new FormData();
            formData.append('productImage', this.state.file);

        let locationsCount = this.locationsCounter(this.state.locations);

        let item = [{
            code: this.state.code,
            locations: this.state.locations,
            name: this.state.name,
            locationCounts: locationsCount
        }];

        formData.append('item', JSON.stringify(item));
    
        API('addStock', 'POST', formData, false, false)
            .then((response) => {
                console.log(response)
               this.setState({
                name: '',
                fileList: [],
                file: [],
                base64Image: '',
                feedback: {error: false, message: ''},
                view: 'successScreen'
               })
                
            })
            .catch((error) => {
                console.log(error);
                this.setState({
                    feedback: {error: true, message: 'Error Occurred. Item Not Saved.'}
                })
        });
    };

    render() {
        const uploadProps = { action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76', onChange: this.onFileChange, multiple: false, beforeUpload: this.beforeUpload, fileList: this.state.fileList };

        return (
            <div>
                {
                    this.state.view === 'addStock' ?
                        <Row className="form-container">
                        <Col>
                            <Divider><Tag color="#B49328">ADD STOCK</Tag></Divider>
                        </Col>
    
                        <Col>
                            <Form>
                            <Input 
                                style={{ padding: '10px'}} 
                                prefix={<Icon type="barcode" style={{ color: 'rgba(0,0,0,.25)', padding: '3px' }} />}
                                placeholder="Code"
                                onChange={this.onCodeChange}
                            />
    
                            <Row>
                                <Divider style={{ margin: 0 }}> Storage Location </Divider>
                            </Row>
    
                            <Row className="padding10">
                                {this.state.locationList.map((location) => {
                                   return location.value.includes('Storage') ?  <Col span={12} key={location.value}> <Checkbox.Group options={[location]} onClick={this.onLocationChange}/> </Col> : null ;
                                })}
                            </Row>
    
                            <Row>
                                <Divider style={{ margin:0 }}>Other Locations</Divider>
                            </Row>
    
                            <Row className="padding10">
                                {this.state.locationList.map((location) => {
                                   return location.value.includes('Storage') ? null : <Col span={12} key={location.value}> <Checkbox.Group options={[location]} onClick={this.onLocationChange} /> </Col>;
                                })}
                            </Row>
    
                            <Input
                                style={{ padding: '10px'}}
                                prefix={<FontAwesomeIcon icon={faWineBottle} style={{ color: 'rgba(0,0,0,.25)', padding: '3px' }} />}
                                placeholder="Wine Name"
                                onChange={this.onNameChange}
                            />
                            </Form>
                        </Col>
    
                        <Col>
                        <Upload {...uploadProps} style={{ padding: "10px"}} showUploadList={false}>
                                <Button style={{width: '100%'}}>
                                    <Icon type="upload" /> Click to Upload
                                </Button>
                            </Upload>
                        </Col>
    
                        <Col className="tag-container">
                            {
                                this.state.feedback.message.length > 0 ?
                                    this.state.feedback.error ? 
                                        <Tag color="red"> { this.state.feedback.message} </Tag>
                                        : 
                                        <Tag color="green"> { this.state.feedback.message } </Tag>
                                : false
                            }
                        </Col>
                        
                            <Row className="controls">
                                <Col>
                                    <Button onClick={this.handleSave} style={{width: '100%', marginTop: '20px', zIndex: 1, background: '#B49328', color: '#ffff'}}>Save</Button>
                                </Col>
                                <Col>
                                    <Button onClick={() => this.props.goBack('dashboard')} style={{ width: '100%', background: '#CCCCCC', marginTop: '10px' }}>Back</Button>            
                                </Col>
                            </Row>
                    </Row>
                    : 
                    <div>
                        <Row style={{ padding: '10px' }}>
                        <Col>
                            <Divider><Tag color="#B49328">ADD STOCK</Tag></Divider>
                        </Col>

                            <h2 className="success-text">Successfull Added Stock Item.</h2>

                            <Row className="controls">
                                <Col>
                                    <Button onClick={() => this.setState({ view: 'addStock' })} style={{width: '100%', marginTop: '20px', zIndex: 1, background: '#B49328', color: '#ffff'}}>Add New Item</Button>
                                </Col>

                                <Col>
                                    <Button onClick={() => this.props.goBack('dashboard')} style={{ width: '100%', background: '#CCCCCC', marginTop: '10px' }}>Cancel</Button>            
                                </Col>
                            </Row>

                        </Row>
                    </div>
                }

            </div>
        )
    }
}