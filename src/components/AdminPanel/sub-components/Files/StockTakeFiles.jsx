import React, { Component } from 'react'
import { Row, Col, Button, List, Divider, Tag, Icon, DatePicker, message } from 'antd';
import API from '../../../../resource/API';
import './StockTakeFiles.css';

export default class StockTakeFiles extends Component {
    constructor(props) {
        super(props)
        this.state = {
            files: [],
            originalList: []
        }
    }

    componentWillMount = () => this.getFiles();

    getFiles = () => {
        API('files', 'GET')
        .then((response) => {
            this.setState({
                originalList: response.files,
                files: this.filterFiles(null, response.files)
            })
        })
        .catch((error) => {
            console.log(error);
        })
    };

    filterFiles = (fDate, files = this.state.originalList) => {
        let newDate = fDate ? String(fDate._d).split(" ") : Date().split(" ");
        let day = newDate[2];
        let month = newDate[1];
        let year = newDate[3];

        newDate = `${day}-${month}-${year}`;

        let filteredFiles = files.filter((file) => {
            let date = file.split("_")[1].split(".")[0];
            if (date === newDate) 
                return file
            else return false;
        });

        return filteredFiles;
    };

    onFilterDate = (fDate) => this.setState({ files: this.filterFiles(fDate) });
    
    resetFilter = (original = this.state.originalList) => this.setState({ files: original }); 

    onDownloadFile = (e) => {
        API('downloadFile', 'POST', {file: e.target.value})
            .then((response) => {
                if (response.exists) {
                    window.open(response.filePath)
                }
            })
            .catch((error) => {
                console.log(error);
                message.error('Unable to Download File');
            })
    };

    onDeleteFile = (e) => {
        API('deleteFile', 'POST', {file: e.target.value})
            .then(response => {
                if (response.deleted) {
                    message.success('File Deleted.')
                    this.getFiles()
                }
            })
            .catch((error) => {
                message.error('Unable to Delete File');
            })
    }

    render() {
        return (
           <Row>
                <Col>
                    <Divider><Tag color="#B49328">Stocktake Spreadsheets</Tag></Divider>
                </Col>
                
                <Col style={{ padding: '10px' }}>
                    <DatePicker onChange={this.onFilterDate} placeholder="Search File By Date" size="large" style={{ width: '100%' }} allowClear={true}/>
                </Col>
               
                <Col>
                    <List 
                        dataSource={this.state.files}
                        renderItem={file => (
                            <List.Item actions={[<Button value={file} onClick={this.onDownloadFile}><Icon type="download" /></Button>, 
                                                 <Button value={file} onClick={this.onDeleteFile} type="danger"><Icon type="delete" /></Button>]}>
                                <List.Item.Meta
                                    avatar={
                                    <Icon type="file"/>
                                    }
                                    title={ file.split("_")[0] }
                                    description={file.split("_")[1].split(".")[0]}
                                />
                            </List.Item>
                        )}
                    />
               
                </Col>

                <Row style={{ position: 'fixed', bottom: 0, left: '1px', width: '100%', padding: '10px' }}>
                    <Col>
                        <Button onClick={() => this.props.goBack('dashboard')} style={{ width: '100%', background: '#CCCCCC', marginTop: '10px' }}>Back</Button>            
                    </Col>
                </Row>
           </Row>
        )
    }
}

// {/* <DatePicker size="large" /> */}
