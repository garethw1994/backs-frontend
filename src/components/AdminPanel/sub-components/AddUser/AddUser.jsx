import React, { Component } from 'react'
import './AddUser.css';
import { Row, Button, Col, Form, Input, Divider, Icon, Select, message, Tag } from 'antd';
import API from '../../../../resource/API';

export default class AddUser extends Component {
    constructor(props) {
        super(props)
        this.state = {
            username: '',
            password: '',
            role: ''
        }
    }

    onUsernameChange = (e) => this.setState({ username: e.target.value });

    onPasswordChange = (e) => this.setState({ password: e.target.value });

    onRoleChange = (role) => this.setState({  role  });

    onSaveUser = () => {
        let userDetails = {
            username: this.state.username,
            password: this.state.password,
            role: this.state.role
        };

        console.log(userDetails);

        API('addUser', 'POST', userDetails)
            .then((result) => {
                console.log(result);
                message.success(result.message);
            })
            .catch(error => message.error('Unable To Add User.'));
    };

    render() {
        return (
                <Row>
                    <Col>
                        <Divider><Tag color="#B49328">ADD USER</Tag></Divider>
                    </Col>
                    
                  <Col>
                        <Form>
                            <Input 
                                style={{ padding: '10px'}} 
                                prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)', padding: '3px' }} />}
                                placeholder="Username"
                                onChange={this.onUsernameChange}
                            />
    
                            <Input.Password 
                                style={{ padding: '10px' }}
                                prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25', padding: '3px' }}/>}
                                placeholder="Password"
                                
                                onChange={this.onPasswordChange}
                            />

                            <Select style={{ padding: '10px', width: '100%' }} placeholder="Please select role" onChange={this.onRoleChange}>
                                <Select.Option key="admin" value="admin">Admin</Select.Option>
                                <Select.Option key="stocktaker" value="stocktaker">Stock Taker</Select.Option>
                            </Select>
                        </Form>
                    </Col>

                    <Row style={{ position: 'fixed', bottom: 0, left: '1px', width: '100%', padding: '10px' }}>
                        <Col>
                            <Button onClick={this.onSaveUser} style={{width: '100%', marginTop: '20px', zIndex: 1, background: '#B49328', color: '#ffff'}}>Add User</Button>
                        </Col>

                        <Col>
                            <Button onClick={() => this.props.goBack('dashboard')} style={{ width: '100%', background: '#CCCCCC', marginTop: '10px' }}>Back</Button>            
                        </Col>
                    </Row>  
            </Row>         
        )
    }
}
