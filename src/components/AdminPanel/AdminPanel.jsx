import React, { Component } from 'react'
import util from '../../resource/utilities';
import './AdminPanel.css';
import { Row, Col, Button, Icon } from 'antd';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faWineBottle } from '@fortawesome/free-solid-svg-icons'
import AddStock from './sub-components/AddStock/AddStock';
import LoginInfo from '../Views/loginInfo';
import UserList from './sub-components/UserList/UserList';
import AddUser from './sub-components/AddUser/AddUser';
import StockTakeFiles from './sub-components/Files/StockTakeFiles';

// import UserList from './sub-components/UserList';

export default class AdminPanel extends Component {
    constructor(props) {
        super(props)
        this.state = {
            user: [],
            userList: [],
            selectedUser: [],
            process: "",
            collapsed: false,
            selectedView: "stockchart"
        }
    }

    componentWillMount = () => {
      let token = this.props.token;
       
      let userObj;
  
      if (token) {
        userObj = util().parseJwt(token);
        userObj.lastLogin = `${new Date(userObj.lastLogin).toDateString()} ${new Date(userObj.lastLogin).toTimeString().split("G")[0]}`;
        userObj.currentLogin = `${new Date(userObj.currentLogin).toDateString()} ${new Date(userObj.currentLogin).toTimeString().split("G")[0]}`;
      };

      this.setState({
        // userList: res.users,
        user: userObj,
        process: 'dashboard'
      });
    }

    changeProcess = (name) => this.setState({ process: name });

  render() {
      return (
          <div>
            {
              this.state.process === 'dashboard' ? 
              <div>
              <Row>
                <LoginInfo userObj={this.state.user} logout={this.props.route}/>                  
              </Row>

              <Row gutter={16} style={{ margin: 0 }}>
                <Col className="gutter-row admin-box" span={6}>
                    <div className="gutter-box">
                      <Button className="admin-box-button" onClick={() => this.changeProcess('addStock')}>
                        <div className="admin-icon-container">
                        <FontAwesomeIcon icon={faWineBottle} className="admin-box-icon"/>
                        </div>
                        Add Wine Item
                      </Button>
                    </div>
                </Col>
  
                <Col className="gutter-row admin-box" span={6}>
                    <div className="gutter-box">                     
                      <Button className="admin-box-button" onClick={() => this.changeProcess('userList')}>
                        <div className="admin-icon-container">
                         <Icon type="user" className="admin-box-icon"/>
                        </div>
                        List Users
                      </Button>
                    </div>
                </Col>

                <Col className="gutter-row admin-box" span={6}>
                    <div className="gutter-box">        
                      <Button className="admin-box-button" onClick={() => this.changeProcess('addUser')}>
                        <div className="admin-icon-container">
                         <Icon type="plus" className="admin-box-icon"/>
                        </div>
                        Add User
                      </Button>
                    </div>
                </Col>

                <Col className="gutter-row admin-box" span={6}>
                    <div className="gutter-box">
                      <Button className="admin-box-button" onClick={() => this.changeProcess('stocktakeFiles')}>
                        <div className="admin-icon-container">
                         <Icon type="file" className="admin-box-icon"/>
                        </div>
                        Stocktake Files
                      </Button>
                    </div>
                </Col>
              </Row>
              </div>
              : this.state.process === 'addStock' ?
                <AddStock goBack={this.changeProcess}/>
              : this.state.process === 'userList' ?
                <UserList goBack={this.changeProcess} user={this.state.user}/>
              : this.state.process === 'addUser' ?
                <AddUser goBack={this.changeProcess}/>
              : this.state.process === 'stocktakeFiles' ?
                <StockTakeFiles goBack={this.changeProcess}/>
              : false
            }
          </div>     
      )
  }
}