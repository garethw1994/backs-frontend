import React, {Component} from 'react'
import LoginInfo from '../Views/loginInfo';
import util from '../../resource/utilities';
import './StockTakerPanel.css';
import WineLocations from './Screens/Locations';
import WineList from './Screens/WineList';
import UpdateWine from './Screens/UpdateWine';
import { Button, Row, Col, message } from 'antd';
import API from '../../resource/API';


export default class StockPanel extends Component {
  constructor(props) {
    super(props)
    this.state = {
      user: [],
      process: "init",
      location: '',
      wine: [],
      subLocations: ['Premium Range','Family Reserve','Black Label Range', 'Kosher Range', 'Thread Lightly Range', 'Brandy', 'Magnums'],
      submitDisabled: false
    }
  }

  componentWillMount = () => {
    let token = this.props.token;
   
    let userObj;

    if (token) {
      userObj = util().parseJwt(token);
      userObj.lastLogin = `${new Date(userObj.lastLogin).toDateString()} ${new Date(userObj.lastLogin).toTimeString().split("G")[0]}`;
      userObj.currentLogin = `${new Date(userObj.currentLogin).toDateString()} ${new Date(userObj.currentLogin).toTimeString().split("G")[0]}`;
    };

    this.setState({
      user: userObj
    })
  }

  onChangeView = (e) => this.setState({ process: e.target.name });

  handleLocationSelect = (location) =>  {
    if (location.toLowerCase() === 'storage room') {
      this.setState({ location, process: 'subcategories' })
    } else {
      this.setState({ location, process: 'winelist' })
    }
  };

  handleWineSelect = (wine) => this.setState({ wine, process: 'editWine' });

  handleSubmit = () => {
    API('buildExcel', 'GET')
      .then((response) => {
        message.success('Stock Data Submitted!');
        this.setState({ submitDisabled: true });
      })
      .catch((error) => {
        message.error('Unable To Submit Stock Data.');
      })
  };

  render() {
    return (
    <div className="main-view">
      {
        // INIT PROCESS
        this.state.process === "init" ?
          
          <div className="component-containers">
            <LoginInfo userObj={this.state.user} changeRoute={this.onChangeView} logout={this.props.route}/>      

            <WineLocations wineChange={this.handleLocationSelect}/> 

          <Row className="submit-container">
              <Button onClick={this.handleSubmit} className="submitStockButton" disabled={this.state.submitDisabled}>Submit Stock Data</Button>
          </Row>

          </div>
      : this.state.process === 'winelist' ?
            <WineList location={this.state.location} changeRoute={this.onChangeView} wineSelection={this.handleWineSelect}/>
      : this.state.process === 'subcategories' ?
            <WineLocations wineChange={this.handleLocationSelect}  changeRoute={this.onChangeView} subLocations={this.state.subLocations}/> 
      : this.state.process === 'editWine' ?
            <UpdateWine wine={this.state.wine} changeRoute={this.onChangeView} location={this.state.location}/>     
      : false
      }
    </div>
    )
  }
}

  