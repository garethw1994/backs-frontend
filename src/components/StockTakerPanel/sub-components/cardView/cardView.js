import React from 'react';
import { Card, Row, Col } from 'antd';
import './cardView.css';

export default ({ wines, host, selectedWine}) => {
    console.log(wines)
 return <Row>
             {wines.map((wine) => {
                return <Col className="gutter-row stocktaker-box" span={9} key={wine.code} style={{ margin: '5px' }}>
                        <div className="gutter-box">
                            <Card onClick={() => selectedWine(wine)} hoverable style={{ width: 'auto' }} 
                                cover={<div className="div-avatar">
                                            <img alt={wine.productImage} src={`${host}${wine.productImage}`} style={{ maxHeight: '100%', maxWidth: '100%' }}/>
                                        </div>}>
                                <Card.Meta title={wine.name} />
                            </Card>
                        </div>
                       </Col>
            })}
        </Row>

};
