import React from 'react';
import { List, Avatar } from 'antd';
import './listview.css'

export default ({ wines, host, selectedWine }) => {
   return  <List
            style={{ padding: '10px', marginBottom: '40px' }}
            itemLayout="horizontal"
            dataSource={wines}
            renderItem={wine => (
                <List.Item style={{ width: '255px' }}>
                    <List.Item.Meta 
                        avatar={<Avatar src={`${host}${wine.productImage}`} style={{borderRadius: 0, display: 'flex', justifyContent: 'center', height: '80px'}}/>}
                        title={wine.name}
                        description={`Click to view more`}
                        onClick={() => selectedWine(wine)}
                    />
        </List.Item>
)}
> 
</List>            
}