import React from 'react';
import util from '../../../resource/utilities';
import { Input, Form } from 'antd';

const formItemStyle = {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    width: '100%'
};

export default () => {
    let winePacks = (item, packChange) => {
        let packs = [item].map(line => {
            let packs = line.packSize;
            return packs.map((pack) => {
                return <Form.Item key = { util().randomKey() }
                label = { pack.type }
                style = { formItemStyle }
                name = { pack.type } >

                    <Input key = { pack.type + 'uid' }
                maxLength = { 25 }
                size = "large"
                min = { 0 }
                max = { 100000 }
                name = { pack.type }
                defaultValue = { pack.count }
                onChange = { packChange }
                /> 

                </Form.Item >
            })
        });

        return packs;
    };

    let wineDetails = (item) => {
        let details = [item].map((line) => {
            let keys = Object.keys(line);
            let filteredKeys = util().wineFilter(keys);
            let updatedKeys = filteredKeys.map((key) => {
                return util().stringManipulation(key);
            });
            let mapped = util().mapObject(updatedKeys, line);
            return mapped.map((value) => {
                let KEY = Object.keys(value);
                if (KEY[0] !== 'Packsize') {
                    return <Form.Item key = { util().randomKey() }
                    label = { KEY }
                    style = { formItemStyle } >
                        <Input key = { KEY + 'uid' }
                    defaultValue = { value[KEY] }
                    placeholder = "default size"
                    disabled />
                        </Form.Item>
                }
                return false;
            })
        });

        return details;
    };


    return {
        winePacks,
        wineDetails
    }
}