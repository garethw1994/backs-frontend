import React, { Component } from 'react'
import headings from '../../../resource/displays/headings';
import { Form, Card, Divider, Icon, Input, Button, message, Row } from 'antd';
import '../StockTakerPanel.css';
import util from '../../../resource/utilities';
import API from '../../../resource/API';

const formItemStyle = {
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  width: '100%'
};

export default class UpdateWine extends Component {
    constructor(props) {
        super(props)
        this.state = {
            wine: [],
            currentLocation: '',
            packSizes: [{
              bottles: 0,
              packs: 0,
              boxes: 0
            }]
        }
    }

    componentWillMount = () => {
        // filter wine 
        this.setState({
            wine: this.props.wine,
            currentLocation: this.props.location
        })
    };

    handleCountChange = (e) => {
      let packs = this.state.packSizes;
      let pack = e.target.name;
      let value = Number(e.target.value);

      message.destroy();

      console.log(value)

      if (value || value === 0) { 
        packs[pack] = value; 
      }
        else message.error('Enter a valid Stock Value.');

      console.log(this.state.packSizes)
      // this.setState({ 
      //   packSizes: packs  
      // })
    };
    
    updateStock = () => {
      let updatedPacks = this.state.packSizes;
      let bottles = updatedPacks['bottles'];
      let packs = updatedPacks['packs'];
      let boxes = updatedPacks['boxes'];

      let currentWine = this.state.wine;

      currentWine.locationsCount.map((details) => {
        if (details.location === this.state.currentLocation) {
          details.packSize.map((pack) => {
            let type = pack.type;

            if (type === 'bottles' && bottles !== undefined) pack.count = bottles;
            if (type === 'packs' && packs !== undefined) pack.count = packs;
            if (type === 'boxes' && boxes !== undefined) pack.count = boxes;
            
            return true;
          });
        }

        return false;
      })

      // call api to update item
        API('updateStock', "POST", [{item: currentWine}])
        .then((res) => {
          message.success(res.status);
          this.props.changeRoute({ target: { name: 'winelist'} } );
        })
        .catch((error) => {
          try {
            console.log(error);
            message.destroy();
            message.error(error.responseJSON.message);
          } catch {
            message.destroy();
            message.error('Internal Error Occurred. Try Again.')
          };
        })
    };

  render() {
     let wine = this.state.wine;
  
    return (
      <div>
        <div className="editable-container">
                {
                    wine !== undefined ?
                    <div>
                    <Form layout="horizontal">
                            <Card size="medium" title={wine.name} style={{width: '100%', padding: '5px',}} bordered={false} extra={ <Icon type="info-circle" theme="filled" style={{color: "#B49328"}}/>}>
                         
                                <Divider />
                          {
                            wine.locationsCount.map((details) => {
                              if (details.location === this.state.currentLocation) {
                               return details.packSize.map((pack) => {
                                  let type = pack.type;
                                  let count = pack.count;

                              return <div key={util().randomKey()}>
                                      <Form.Item key ={util().randomKey()} label={util().firstLetterUppercase(type)} style={formItemStyle} name={type}>
                                        <Input onChange={this.handleCountChange} key={util().randomKey()+'uid'} maxLength={25} size='large' min={0} max={100000} name={type} defaultValue={count} /> 
                                      </Form.Item>
                                      <Divider />
                                     </div>
                                })
                              } 
                              
                            return false;                            
                            })
                          }

                        <Row style={{
                          position: 'fixed',
                          bottom: 0,
                          width: '100%',
                          left: '1px',
                          padding: '10px'
                        }}>
                          <Button style={{ marginBottom: '5px', background: '#B49328', color: '#ffff', width: '100%' }} onClick={this.updateStock}>Update</Button>
                          <Button style={{ width: '100%', background: '#d9d9d9' }} onClick={this.props.changeRoute} name="winelist">Cancel</Button>
                        </Row>
                      </Card> 
                    </Form>
                    </div>
                    : 
                    <div>
                        { headings().subHeading('No Details Found') }

                        <Button style={{ width: '100%', background: '#d9d9d9' }} onClick={this.props.changeRoute} name="winelist">Cancel</Button>
                    </div>
                }
          </div>
      </div>
    )
  }
}
