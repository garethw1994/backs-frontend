import React, { Component } from 'react'
import API from '../../../resource/API';
import { Button } from 'antd';
import headings from '../../../resource/displays/headings';

export default class WineLocations extends Component {
    constructor(props) {
        super(props)
        this.state = {
            locations: [],
            hcLocations: false
        }
    };

    getLocations = (location = 'allLocations') => {
        // GET WINE LOCATIONS
        API(location, 'GET')
        .then((result) => {
          console.log(result);
          
            this.setState({
                locations: result
            })
        })
        .then((error) => console.log(error))
    };

    componentWillMount = () => {
      if (this.props.subLocations) {
        this.setState({
          locations: this.props.subLocations,
          hcLocations: true
        })
      } else {
        this.getLocations()
      }
    } 

    buttonList = (name, label) => <Button value={name} onClick={() => this.props.wineChange(name)} style={{width: '100%', marginBottom: '5px'}} key={Math.random() * 1000}>
                                    { label ? label : name}
                                  </Button>

  render() {
    return (
      <div style={{ padding: '10px' }}>
          {
              headings().subHeading('Locations')
          }
          {
              this.state.locations.length > 0 ?
                this.state.locations.map((location) => {
                  if (this.state.hcLocations) {
                    // modify sub locations
                    let name = `Storage Room - ${location}`;

                    return <div key={location}>
                              { this.buttonList(name, location) }
                        
                              <div style={{padding: '10px', bottom: 0, position: 'fixed', width: '100%', background: '#ffff', left: '1px'}}>
                                <Button style={{width: '100%', background: '#d9d9d9'}} onClick={this.props.changeRoute} name="init">Back</Button>    
                              </div>
                            </div>
                  } else {
                    // find storage location and set it to be just storage
                    if (location.includes('Storage Room')) {
                      let trimmedName = location.split("-")[0].trim();
                      return this.buttonList(trimmedName) 
                  } else {
                      return this.buttonList(location) 
                    }
                  }
                })
              : false
          }
      </div>
    )
  }
}