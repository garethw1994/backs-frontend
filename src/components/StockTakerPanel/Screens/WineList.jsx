import React, { Component } from 'react'
import API from '../../../resource/API';
import { Button, Radio, Icon, Divider, Tag } from 'antd';
import headings from '../../../resource/displays/headings';
import ListDisplay from '../sub-components/listView/listView';
import CardDisplay from '../sub-components/cardView/cardView';

import util from '../../../resource/utilities';

export default class WineList extends Component {
    constructor(props) {
        super(props)
        this.state = {
            currentLocation: '',
            wines: [],
            baseUrl: '',
            view: 'list'
        }
    }

    componentWillMount = () => {
     let location = this.props.location;
    
     // get view preference if any
     let view = util().localStorageRetrieve('viewPref');
     API(`location/${location}`, 'GET')
        .then((result) => {
            let wines = result.response;
            let origin = window.location.origin.includes('localhost') ? 'http://localhost:3001/' : `${window.location.origin}/`;

            this.setState({
                currentLocation: location,
                wines,
                baseUrl: origin,
                view
            })
        })
        .catch((error) => console.log(error))
    };

    handleViewChange = (e) => {
        // set value in local storage
        util().localStoragePersist('viewPref', e.target.value);

        this.setState({ view: e.target.value })
    }

    backButton = (name = 'init') => {
        return <div style={{padding: '10px', bottom: 0, position: 'fixed', width: '100%', background: '#ffff'}}>
                 <Button style={{width: '100%', background: '#d9d9d9'}} onClick={this.props.changeRoute} name={name}>Back</Button>    
               </div>
    }
    
  render() {
    return (
      <div>
        {headings().subHeading('Wine List')}
       
        <Radio.Group value={this.state.view} onChange={this.handleViewChange}>
                <Radio.Button value="list"><Icon type="unordered-list" /></Radio.Button>
                <Radio.Button value="card"><Icon type="appstore" /></Radio.Button>
        </Radio.Group>

        {
            this.state.wines.length > 0 ?
                this.state.view === 'list' ?
                    <ListDisplay wines={this.state.wines} host={this.state.baseUrl} selectedWine={this.props.wineSelection}/>
                : 
                    <CardDisplay wines={this.state.wines} host={this.state.baseUrl} selectedWine={this.props.wineSelection} />
            : <div>
                <Divider> <Tag color="#B49328">No Wines Found</Tag> </Divider>
            </div>
        }

        {
            this.state.currentLocation.includes('Storage') ?
                this.backButton('subcategories')
            :
                this.backButton()
        }
        
      </div>
    )
  }
}
