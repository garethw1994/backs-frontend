import React from 'react'
import { withRouter } from 'react-router-dom';
import './Header.css';
import { Row, Col, Avatar } from 'antd';
import util from '../../resource/utilities';
import Logo from '../../assets/bb-full-220.png'

const Header = ({route, user }) => {
  console.log(user)
  return (
    <div className="header">
          {
            route === 'login' ?
            <div>
              <Row>
                <Col span={6}></Col>
                <Col span={12} style={{
                  display: "flex",
                  justifyContent: "center"
                }}>
                <img src={Logo} width="120px" height="60px" alt={<h5 className="app-heading">Backsberg</h5>}></img>

                </Col>

                <Col span={6}></Col>
              </Row>
            </div>
            :
            route === 'logged_in' ?
            <div>
              <Row>
              <Col span={6} 
              style={{
                display: "flex",
                alignItems: "center",
                height: "60px",
                width: "23%"
              }}
              >
              </Col>
           
              <Col span={12} style={{
                display: "flex",
                justifyContent: "center"
              }}>
               <img src={Logo} width="120px" height="60px" alt={<h5 className="app-heading">Backsberg</h5>}></img>
              </Col>
              <Col span={8}
              style={{
                display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                  height: "60px",
                  width: "23%"
                }}
                >
                <Avatar icon="user" size="small"/> 

                  <span style={{
                    padding: '5px',
                    color: "#B49328",
                    fontSize: '12px'
                  }}>
                  
                  { util().firstLetterUppercase(user.username) }
                  
                  </span>
                        
              </Col>
            </Row>
            </div>   
            : false
          }
    </div>
  )
};

export default withRouter(Header);