import React, { Component } from 'react'
import API from '../../resource/API';
import { message } from 'antd';
import Header from '../Header/Header';
import util from '../../resource/utilities';
import StockPanel from '../StockTakerPanel/StockPanel';
import AdminPanel from '../AdminPanel/AdminPanel';

export default class Dashboard extends Component {
  constructor(props) {
    super(props)
    this.state = {
      token: '',
      locations: [],
      user: [],
      screen: ''
    }
  };


  componentWillMount = () => {
       // connect to chat
    let token = window.location.search.split("=")[1];
    let user = util().getUser(token);
  
    //get all storage locations
       API('allLocations', 'GET')
         .then((locations) => {
            this.setState({
              locations: locations
            })
         })
    .catch((error) => {
       try {
         message.destroy();
         message.error(error.responseJSON.message);
       } catch {
         message.destroy();
         message.error('Internal Error Occurred. Try Again.')
       };
     })

      this.setState({
        screen: user.role,
        token: token,
        user: user
      })
  };

  handleNotiCount = (step) => {
    switch (step) {
      case "remove":
        util().localStorageRemove('noti-count');
        this.setState({
          messageCount: 0
        })
        break;    
      default:
        break;
    }
  }

  switchRoute = (e, value) => {
    let val = '';
    if (!e) {
      val = value;
    } else {
      val = e.target.name;
    }
    
    switch (val) {
      case "logout":
        let result = util().localStorageRemove('token');
        console.log(result)
        if (result) {
          message.info('Logged Out. Please login again.');
          this.props.history.push('login');
        }
        break;
      case "login":
        this.props.history.push('login');
        break;
      default:
        this.setState({
          screen: val
        })
        break;
    }
}

  render() {
    return (
      <div>
        <Header route='logged_in'  user={this.state.user} switchRoute={this.switchRoute} />
        {
          this.state.screen === 'admin' ?
           <AdminPanel token={this.state.token} route={this.switchRoute} messageCount={this.state.messageCount} user={this.state.user} />
          : this.state.screen === 'stocktaker' ?
            <StockPanel token={this.state.token} locations={this.state.locations} route={this.switchRoute} messageCount={this.state.messageCount} user={this.state.user} />
          : false
        } 
      </div>
    )
  }
}
