import React, { Component } from 'react'
import  API  from '../../resource/API';
import { Button, Layout, Col, Row, Form, message, Input, Icon, Divider } from 'antd';
import util from '../../resource/utilities';
import Header from '../Header/Header';
import './Login.css';

export default class Login extends Component {
    constructor(props) {
        super(props)
      this.state = {
        username: '',
        password: ''
      }
    }
    
    handleRouteChange = (e) => {
      this.props.history.push(e.target.name)
    };

    // update username state with new value
    handleUsernameTyping = (e) => this.setState({ username: e.target.value.toLowerCase() });

    // update password state with new value
    handlePasswordTyping = (e) => this.setState({ password: e.target.value });

    // handle form submit
    formSubmit = () => {
      let credentials = { username: this.state.username, password: this.state.password };
      
      if (credentials.username === "" || credentials.password === "") {
        message.error('Please enter a username and password')
      } else {
            // call API
            message.loading('logging in...', 2)
            .then(() => {
              API('login', 'POST', credentials) 
                .then((res) => {
                  console.log(res)
                  message.destroy();
                  message.success(res.message);
                  // persist token to localstorage
                  let token = res.token;

                  util().localStoragePersist('token', token);
                  
                  this.props.history.push(`dashboard?uid=${token}`);
                })
                .catch((error) => {
                  try {
                    console.log(error);
                    message.destroy();
                    message.error(error.responseJSON.message);
                  } catch {
                    message.destroy();
                    message.error('Internal Error Occurred. Try Again.')
                  };
                })
            })
      }
   
  }

  render() {
    return (
        <Layout>
              <Header route='login'/>

              <Layout className="main-layout">
                  <Col span={1} className="width-100">
                    <Row><h4 className="login-heading">Login</h4></Row>
                  </Col>
                  <Divider />

                  <Col span={1} className="width-100">
                    <Row>
                     <Form className="width-100">
            
                    <div className="input-holder">
                    <Input 
                          className="input-field"
                          placeholder="Input username"
                          prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }}/>}
                          value={this.state.username}
                          onChange={this.handleUsernameTyping}
                          size="large"
                      />
                    </div>
                      
                    
                      <div className="input-holder">
                      <Input.Password 
                          className="input-field"
                          placeholder="Input password"
                          prefix={<Icon type="lock" style={{color: 'rgba(0,0,0,.25'}}/>}
                          value={this.state.password}
                          onChange={this.handlePasswordTyping}
                          size="large"
                      />         
                      </div>
             
                        <Divider />

                        <div>
                          <Button className="login-button" type="primary" onClick={this.formSubmit} block>Login</Button>                      
                        </div>
                     </Form>
                    </Row>
                  </Col>
              </Layout>
        </Layout>
    )
  }
}