import React, { Component } from 'react'
import util from '../../resource/utilities';
import API from '../../resource/API';
import { message, notification } from 'antd';
import StockView from './stockview';
import EditableForm from './editableForm';
import Lists from './list';

let errors = "";
let item = []
export default class Locations extends Component {
 constructor(props) {
   super(props)
   this.state = {
     username: '',
     role: '',
     token: '',
     loc: '',
     currentView: [],
     status: '',
     searchedItems: [],
     itemInEdit: [],
     errors: ''
   }
 }
  componentWillMount = () => {
    let user = util().parseJwt(this.props.token);
    this.setState({
      username: user.username,
      role: user.role,
      token: this.props.token
    })
  }

  getWines = (location) => {
    API(`location/${location}`, 'GET')
    .then((stock) => {
      console.log(stock)
      this.setState({
        loc: location,
        currentView: stock.response,
        status: 'view'
      })
    })
    .catch((error) => {
      try {
        console.log(error);
        message.destroy();
        message.error(error.responseJSON.message);
      } catch {
        message.destroy();
        message.error('Internal Error Occurred. Try Again.')
      };
    })
  };

  editStock = (wine) => {
    this.setState({
      status: 'edit',
      itemInEdit: wine
    })
  };

  packChange = (event) => {
    let packName = event.target.name;
    let updatedCount = Number(event.target.value);
    let currentItem = this.state.itemInEdit;

    if (!isNaN(updatedCount) && updatedCount !== 0) {
         currentItem.packSize.map(item => {
           if (item.type === packName) {
             item.count = updatedCount
           };
           return false;
         });

         errors = ''

         item = currentItem;

         return true;
    } else if (errors === '') {
      errors = "Invalid Input"
    }
  }

  handleUpdate = () => {
    let itemToUpdate = item;

    if (errors !== '') {
      notification["error"]({
        message: 'Input Error',
        description: 'Make sure you entering numbers when doing stock count. Not characters.',
      });
    } else {
 // call api to update item
 API('updateStock', "POST", [{item: itemToUpdate}])
 .then((res) => {
   console.log(res)
   message.success(res.status);
   this.setState({
     status: 'view'
   })
 })
 .catch((error) => {
   try {
     console.log(error);
     message.destroy();
     message.error(error.responseJSON.message);
   } catch {
     message.destroy();
     message.error('Internal Error Occurred. Try Again.')
   };
 })
    }
  };

  handleCancel = () => {
    this.setState({
      status: 'view'
    })
  };

  handleSearch = (e) => {
    let searchValue = e.target.value;
    let currentWines = this.state.currentView;

    if (searchValue !== "") {
      let searchList = currentWines.filter((wine) => {
        if (wine.name.toLowerCase().includes(searchValue.toLowerCase())) {
          return wine;
        } 
        return false;
      })

      this.setState({
        searchedItems: searchList,
        status: 'search'
      })
    } else {
      this.setState({
        searchItems: [],
        status: 'view'
      })
    }
  };

  render() {
    return (
      <div style={{
        maxWidth: "100%",
        minWidth: "50%"
      }}>
         <Lists lists={this.props.locations} buttonValue='Wine List' cb={this.getWines} cmp="locations"/>
          {
            this.state.status === 'view' ?           
              <StockView stock={this.state.currentView} search={this.handleSearch} location={this.state.loc} update={this.editStock}/>
            :
            this.state.status === 'search' ?
              <StockView stock={this.state.searchedItems} search={this.handleSearch} location={this.state.loc} update={this.editStock}/>          
            :
            
            this.state.status === 'edit' ?
              <EditableForm 
              item={this.state.itemInEdit} 
              location={this.state.loc} 
              packChange={this.packChange}
              handleEdit={this.handleUpdate}
              cancel={this.handleCancel}
              />
            : false
          }
      </div>
    )
  }
}
