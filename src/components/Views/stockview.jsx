import React from 'react'
import List from './list';
import headings from '../../resource/displays/headings';
import { Input, Icon } from 'antd';

const StockView = ({stock, location, update, search }) => {
   return <div>{
           stock.length > 0 ?
             <div>
                 { headings().subHeading(location)}
                  <div style={{
                    display: 'grid',
                    justifyContent: 'center'
                  }}>
                    <Input 
                      placeholder="Search wine"
                      prefix={<Icon type="search" style={{color: 'rgba(0,0,0,.25'}} />}
                      onChange={search}
                      size="large"
                    />
                  </div>
                  <span style={{
                    display: "grid",
                    justifyContent: "center",
                    fontSize: "12px",
                    padding: "7px",
                    color: "lightgrey"
                  }}>Click/Tap on wine item below</span>
                  <List lists={stock} buttonValue="Update Stock" cb={update} cmp="stockview" />
           
             </div>
               :
               <div>
               { headings().subHeading(location)}
               <div style={{
                 display: 'grid',
                 justifyContent: 'center'
               }}>
               <Input 
               placeholder="Search wine"
               prefix={<Icon type="search" style={{color: 'rgba(0,0,0,.25'}} />}
               onChange={search}
               size="large"
             />
             </div>
              <div style={{
                display: 'flex',
                justifyContent: 'center'
              }}>
              <p style={{
                fontSize: '18px',
                padding: '40px',
                fontFamily: 'serif'
              }}>Wine Not Found :( </p>
              </div>
               </div>
   }</div>
}

export default StockView;