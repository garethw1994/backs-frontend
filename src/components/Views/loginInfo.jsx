import React, { Component } from 'react';
import util from '../../resource/utilities';
import { Collapse, Button, Icon } from 'antd';

const customPanelStyle = {
  background: '#f7f7f7',
  borderRadius: 4,
  marginBottom: 24,
  border: 0,
  overflow: 'hidden',
};

export default class LoginInfo extends Component {
  constructor(props) {
    super(props)
    this.state = {
      header: '',
      role: ''
    }
  };

  componentWillMount = () => {
    this.setState({
      header: `Hi ${util().firstLetterUppercase(this.props.userObj.username)}.`,
      role: this.props.role
    })
  };

  onCollapseChange = (value) => {
    if (!value) {
    this.setState({
      header:` Hi ${util().firstLetterUppercase(this.props.userObj.username)}. Click for more details...`
    })
    } else {
    this.setState({
      header:` Hi ${util().firstLetterUppercase(this.props.userObj.username)}.`
    })
    }
  }
  
  render() {
    return (
      <div>
      {
        this.props.userObj.role === 'stocktaker' ?
            <div>
            <Collapse bordered={false} accordion={true} defaultActiveKey={["1"]} onChange={this.onCollapseChange}>
              <Collapse.Panel key="1" style={customPanelStyle} header={this.state.header}>
                <p>
                 <span style={{padding: '5px'}}><b>Role</b> : {util().firstLetterUppercase(this.props.userObj.role)}</span> <br />
                 <span style={{padding: '5px'}}><b>Last Login</b> : {util().firstLetterUppercase(this.props.userObj.lastLogin)}</span> <br />
                 <span style={{padding: '5px'}}><b>Current Login</b> : {util().firstLetterUppercase(this.props.userObj.currentLogin)}</span>
                </p>

                <Button className="headButtons" name="logout" onClick={this.props.logout}><Icon type="logout"/> Log Out </Button>        
              </Collapse.Panel>           
            </Collapse>
           </div>
        : this.props.userObj.role === 'admin' ?
        <div>
        <Collapse bordered={false} accordion={true} defaultActiveKey={["1"]} onChange={this.onCollapseChange}>
          <Collapse.Panel key="1" style={customPanelStyle} header={this.state.header}>
            <p>
             <span style={{padding: '5px'}}><b>Role</b> : {util().firstLetterUppercase(this.props.userObj.role)}</span> <br />
             <span style={{padding: '5px'}}><b>Last Login</b> : {util().firstLetterUppercase(this.props.userObj.lastLogin)}</span> <br />
             <span style={{padding: '5px'}}><b>Current Login</b> : {util().firstLetterUppercase(this.props.userObj.currentLogin)}</span>
            </p>

            <Button className="headButtons" name="logout" onClick={this.props.logout}><Icon type="logout"/> Log Out </Button>        
          </Collapse.Panel>           
        </Collapse>
       </div>
        : false
    }
      </div>
    )
  }
};

// <Button className="headButtons" name="message" onClick={this.props.changeRoute}><Icon type="message"/> Message </Button>            
