import React from 'react'
import util from '../../resource/utilities';
import { Form, Select, List, Avatar } from 'antd';

const Lists = ({lists, buttonValue, cb, cmp }) => {
  return <ul style={{
    display: "grid",
    justifyContent: "center",
    alignItems: "center",
    margin: 0,
    padding: 0
  }}>
        {
            cmp === 'stockview' ? 
                <List
                    style={{
                        width: "293px"
                    }}
                    itemLayout="horizontal"
                    dataSource={lists}
                    renderItem={item => (
                        <List.Item style={{
                            width: '255px'
                        }}>
                            <List.Item.Meta 
                            avatar={<Avatar src="./../../assets/wineic.png" style={{width: '40px', height: '40px'}}/>}
                            title={item.name}
                            description={`${item.code}`}
                            onClick={() => cb(item)}
                            />
                        </List.Item>
                )}
                > 
                </List>                 
            : cmp === 'locations'?
                <Form>
                    <Form.Item hasFeedback>
                        <Select placeholder="Select Location">
                        {
                            lists.map((loc) => {
                                return <Select.Option key={util().randomKey()} value={loc} onClick={() => cb(loc)}>{loc}</Select.Option> 
                        })
                        }
                        </Select>
                    </Form.Item>
                </Form>
                
            : false
    }
    </ul>
}

export default Lists;

// <List
// itemLayout="horizontal"
// dataSource={data}
// renderItem={item => (
//   <List.Item>
//     <List.Item.Meta
//       avatar={<Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />}
//       title={<a href="https://ant.design">{item.title}</a>}
//       description="Ant Design, a design language for background applications, is refined by Ant UED Team"
//     />
//   </List.Item>
// )}
// />