export default () => {
    const emailValidation = (email) => {
        let lowerEmail = email.toLowerCase();
        // let regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        let regex = /^(([^<>(),;:\s@"]+(\.[^<>(),;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
       
        return regex.test(String(lowerEmail));
    };

    const validPassword = (password) => {
        if (password.length < 8) {
            return 'Password too short'
        };
    };

    const passwordMatch = (passwordOne, passwordTwo) => {
        if (passwordOne !== passwordTwo) {
            return false;
        } else {
            return true;
        }
    };

    const firstLetterUppercase = (word) => {
        let firstChar = word.charAt(0).toUpperCase();
        return `${firstChar}${word.substring(1, word.length)}`
    };

    const parseJwt = (token) => {
        let base64Url = token.split('.')[1];
        let base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');

        return JSON.parse(window.atob(base64));
    };

    const getUser = (token) => {
        let userObj;
          userObj = parseJwt(token);
          userObj.lastLogin = `${new Date(userObj.lastLogin).toDateString()} ${new Date(userObj.lastLogin).toTimeString().split("G")[0]}`;
          userObj.currentLogin = `${new Date(userObj.currentLogin).toDateString()} ${new Date(userObj.currentLogin).toTimeString().split("G")[0]}`;
        return userObj;
    };

    const localStoragePersist = (key, item) => {
            localStorage.setItem(key, item)
    };

    const localStorageRetrieve = (key) => {
        return localStorage.getItem(key);
    };

    const localStorageRemove = (key) => {
        localStorage.removeItem(key);
        
        if (localStorage.getItem(key) === null) {
            return true;
        } else {
            return false;
        }
    };

    const stringManipulation = (text) => {
        let fChar = text.charAt(0).toUpperCase();
        return `${fChar}${text.substring(1, text.length).toLowerCase()}`;
    };

    const wineFilter = (keys) => {
        return keys.filter((key) => {
                        if (key !== '__v') 
                            return key
                        else return false;
                });
    };

    let mapObject = (keys, item) => {
        let mappedObject = [];
    
        for (var oldKey in item) {
            for (var i = 0; i < keys.length; i++) {
                if (oldKey.toLowerCase() === keys[i].toLowerCase()) {
                    let newItem = {};
                    newItem[keys[i]] = item[oldKey]

                    mappedObject.push(newItem);
                }
            };
        };
        return mappedObject;
    };

    let randomKey = () => {
        return `${Math.random() * 10000}_key`
    };

    let reverseString = (text, type) => {
        if (type === "date") {
            let splitArr = text.split("-");
            let newArr = [];

            for (var i = splitArr.length - 1; i >= 0; i--) {
                console.log(i)
                newArr.push(splitArr[i])
            };

            return newArr.join("/")
        }
    }

    return {
        emailValidation,
        passwordMatch,
        validPassword,
        parseJwt,
        localStoragePersist,
        localStorageRetrieve,
        firstLetterUppercase,
        stringManipulation,
        wineFilter,
        mapObject,
        randomKey,
        getUser,
        localStorageRemove,
        reverseString
     }
};