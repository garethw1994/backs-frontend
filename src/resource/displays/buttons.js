import React from 'react';
import { Button } from 'antd';

export default (style) => {
    let cancelButton = (cb) => { return <Button onClick={cb} name="cancel" block>Cancel</Button> }

    let updateButton = (cb) => { return <Button onClick={cb} name="update" block style={style}>Update</Button> }

    let backButton = (cb) => { return <Button onClick={cb} name="back">Back</Button> }

    let loginButton = (cb) => { return <Button onClick={cb} name="login">Login</Button> }

    let logoutButton = (cb) => { return <Button onClick={cb} name="logout">Log Out</Button> }

    return {
        cancelButton,
        updateButton,
        backButton,
        loginButton,
        logoutButton
    } 
}

