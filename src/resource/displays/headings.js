import React from 'react';
import { Card, Icon, Divider } from 'antd';
import '../../components/StockTakerPanel/StockTakerPanel.css';

export default () => {
    let mainHeading = (text) => { return <h2>{text}</h2> }

    let subHeading = (text) => { return <h3 style={{
        textAlign: "center",
        fontSize: "18px",
        textDecoration: "underline",
        fontFamily: "serif",
        color: "#B49328"
    }}>{text}</h3> }
    
    let wineLocationHeading = (location, wine) => {
        return <div className="detail-card-container">
                   <Card size="small" title={location} style={{width: 200}} bordered={false} extra={
                    <Icon type="info-circle" theme="filled" style={{color: "#B49328"}}/>
                   }>
                    <p> <Icon type="idcard" theme="filled" /> {wine.code}</p>
                    <Divider />
                    <p> <Icon type="tag" theme="filled" /> {wine.name}</p>
                    <Divider />
                   </Card> 
                </div>
    } 

    return {
        mainHeading,
        subHeading,
        wineLocationHeading
    } 
}