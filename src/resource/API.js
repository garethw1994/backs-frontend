import $ from 'jquery';

const API = (endpoint, method, data, contentType = 'application/json', stringify = true) => {
    // let baseURl = "https://backsberg.herokuapp.com/"
    let baseURl = "http://localhost:3001/"

    switch (method) {
        case "GET":
            return new Promise((resolve, reject) => {
                return $.get(`${baseURl}${endpoint}`)
                    .done((response) => {
                        resolve(response);
                    })
                    .fail((error) => {
                        console.log(error);
                        reject(error);
                    })
            })
        case "POST":
            return new Promise((resolve, reject) => {
                let url = `${baseURl}${endpoint}`;
                return $.ajax({
                        url: url,
                        data: stringify === true ? JSON.stringify(data): data,
                        method,
                        processData: false,
                        contentType
                    })
                    .done((response) => {
                        console.log(response);
                        resolve(response);
                    })
                    .fail((error) => {
                        console.log(error);
                        reject(error)
                    })
            });
        default:
            break;
    }
};

export default API;