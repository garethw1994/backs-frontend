import React, { Component } from 'react';
import 'antd/dist/antd.css';
import { Route, Switch, Redirect } from "react-router-dom";
import Login from './components/Login/Login';
import Dashboard from './components/Dashboard/Dashboard';

class App extends Component {
  render() {
    return (
      <div className="App" style={{
        height: "93vh"
        // border: "5px solid #EFF1F5"
      }}>
        <Switch>
          <Route path="/login" component={Login} />  
          <Route path="/dashboard" component={Dashboard} />

          <Redirect from="/" exact to="/login" />
        </Switch>
      </div>
    );
  }
}

export default App;
